#' Example of atmosphere output file.
#'
#' A dataset representing the contents of the ENVI-met atmosphere binary file.
#'
#' @format A data frame with 21600 rows and 40 variables:
#' \describe{
#'   \item{x}{Spatial coordinate}
#'   \item{y}{Spatial coordinate}
#'   \item{z}{Spatial
#'   coordinate}
#'   \item{Objects}{Single object IDs to visualize the model domain. Meaning of
#'   the different IDs is stored in LEONARDO Special Layer Definition
#'   Files}
#'   \item{Flow_u}{Wind speed. Vector component along the West-East axis
#'   (+: East, -: West)}
#'   \item{Flow_v}{Wind speed. Vector component along the North-South axis
#'   (+: South, -: North)}
#'   \item{Flow_w}{Wind speed. Vector component along the vertical axis (+: up,
#'   -: down)}
#'   \item{Wind_Speed}{Wind speed. Vector sum over all 3 axis}
#'   \item{Wind_Speed_Change}{Wind speed change in percent referring to the
#'   undisturbed inflow profile at the same height level.}
#'   \item{Wind_Direction}{Wind direction of horizontal component in geographic
#'   reference (0: N..90:E..180:S etc)}
#'   \item{Pressure_Perturbation}{Dynamic pressure as a result of the wind
#'   field calculation. Pressure values will add up over time, use spatial
#'   difference values only if required}
#'   \item{Air_Temperature}{Potential air temperature at reference (and model
#'   default) pressure. For the 3D model, it can be treated like the absolute
#'   air temperature}
#'   \item{Air_Temperature_Delta}{Difference between the local air temperature
#'   and the reference air temperature at inflow at the same height level}
#'   \item{Air_Temperature_Change}{Changes of air temperature compared to the
#'   last _AT_ output file}
#'   \item{Spec._Humidity}{Specific air humidity}
#'   \item{Relative_Humidity}{Relative air humidity (Caution: Depends both on
#'   Specific air humidity and air temperature)}
#'   \item{TKE}{Local Turbulent Kinetic Energy}
#'   \item{Dissipation}{Local dissipation rate of Turbulent Kinetic Energy}
#'   \item{Vertical_Exchange_Coef._Impuls}{Calculated vertical exchange
#'   coefficient for impulse}
#'   \item{Horizontal_Exchange_Coef._Impuls}{Calculated horizontal exchange
#'   coefficient for impulse (At the moment for microscale assumed to be equal
#'   to the vertical exchange coefficient)}
#'   \item{Vegetation_LAD}{One-sided Leaf Area Density (Surface of leaf area
#'   per m³ air)}
#'   \item{Direct_Sw_Radiation}{Available direct solar radiation referring to a
#'   reference surface perpendicular to the incoming sun rays (maximum value
#'   before applying Lamberts' law)}
#'   \item{Diffuse_Sw_Radiation}{Available diffuse solar radiation referring to
#'   a horizontal reference surface}
#'   \item{Reflected_Sw_Radiation}{Availablereflected solar radiation from the
#'   environment referring to a horizontal reference surface}
#'   \item{Temperature_Flux}{Temperature Flux in K from leaf to atmosphere}
#'   \item{Vapour_Flux}{Evaportation and transpiration flux on leaf per leaf
#'   area unit}
#'   \item{Water_on_Leafes}{Liquid water on leaf per leaf area}
#'   \item{Leaf_Temperature}{Temperature Flux in K from leaf to atmosphere}
#'   \item{Local_Mixing_Length}{Local mixing length calculated from TKE model}
#'   \item{Mean_Radiant_Temp.}{The composed radiative fluxes and air temperature
#'   for a standing person}
#'   \item{TKE_normalised_1D}{Local TKE normlized to 1
#'   with 1D reference model}
#'   \item{Dissipation_normalised_1D}{Local TKE
#'   dissiplation normlized to 1 with 1D reference model}
#'   \item{Km_normalised_1D}{Local Km normlized to 1 with 1D reference model}
#'   \item{TKE_Mechanical_Turbulence_Prod.}{Local TKE mechnical production
#'   normalized to 1 with 1D reference model}
#'   \item{Stomata_Resistance}{Actual resistance of stomata to vapour transfer}
#'   \item{CO2}{Atmospheric CO2}
#'   \item{CO2 1}{Atmospheric CO2}
#'   \item{Plant_CO2_Flux}{CO2 Flux at leaf per leaf area unit}
#'   \item{Div_Rlw_Temp_change}{Radiative cooling/heating rate of air due to
#'   longwave radiation divergence}
#'   \item{Building_Number}{Internal Building Number}
#' }
#' @source
#' \url{http://envi-met.info/doku.php?id=filereference:output:atmosphere}
#'
#' @aliases data_atmosphere atmosphere
#' @keywords data atmosphere
"data_atmosphere"

#' Example of buildings output (dynamic subdirectory) file.
#'
#' A dataset representing the contents of the ENVI-met dynamic/buildings binary
#' file.
#'
#' @format A matrix with 64800 rows and 54 variables:
#' \describe{
#'   \item{x}{Spatial coordinate}
#'   \item{y}{Spatial coordinate}
#'   \item{z}{Spatial coordinate}
#'   \item{d}{Type of wall :
#'     \describe{
#'       \item{0}{vertical façade in (yOz) plan (xWall)}
#'       \item{1}{vertical façade in (xOz) plan (yWall)}
#'       \item{2}{horizontal façade (zWall)}
#'     }
#'   }
#'   \item{Wall_shading_flag}{Binary flag indicating if the wall is shaded or
#'   not}
#'   \item{Wall_Temperature_Node_1_outside}{Temperature of the wall at the
#'   interface between the first material and the atmosphere. See:
#'   \url{https://youtu.be/xor6_9-tVRc?t=1114}}
#'   \item{Wall_Temperature_Node_2}{Temperature of the wall in the middle of the
#'   first material.}
#'   \item{Wall_Temperature_Node_3}{Temperature of the wall at the interface
#'   betweeen the first and the second material.}
#'   \item{Wall_Temperature_Node_4}{Temperature of the wall in the middle of the
#'   second material.}
#'   \item{Wall_Temperature_Node_5}{Temperature of the wall at the interface
#'   between the second and the third material.}
#'   \item{Wall_Temperature_Node_6}{Temperature of the wall in the middle of the
#'   third material.}
#'   \item{Wall_Temperature_Node_7_inside}{Temperature of the wall at the
#'   interface between the third material and the air inside the building.}
#'   \item{Building_Sum_Humidity_Flux_at_facade}{Not Enough Information}
#'   \item{Wall_Longwave_radiation_emitted_by_facade}{Longwave radiative flux
#'   emitted by the façade.}
#'   \item{Wall_Wind_Speed_in_front_of_facade}{Wind speed in front of the
#'   façade.}
#'   \item{Wall_Air_Temperature_in_front_of_facade}{Air temperature in front of
#'   the façade.}
#'   \item{Wall_Shortwave_radiation_received_at_facade}{Shortwave radiation
#'   received by the façade.}
#'   \item{Wall_Absorbed_direct_shortwave_radiation}{Absorbed direct shortwave
#'   radiation by the wall.}
#'   \item{Wall_Incoming_longwave_radiation}{Incoming longwave radiation to the
#'   wall.}
#'   \item{Wall_Reflected_shortwave_radiation_facade}{Reflected shortwave
#'   radiation by the wall.}
#'   \item{Wall_Sensible_Heat_transmission_coefficient_outside}{Sensible heat
#'   transmission coefficient on the outside.}
#'   \item{Wall_Longwave_Energy_Balance}{Longwave energy balance of the wall.}
#'   \item{N.N_Building_Temperature_of_building_inside}{Building indoor
#'   temperature.}
#'   \item{Building_Reflected_shortwave_radiation}{Reflected shortwave
#'   radiation by the building.}
#'   \item{Building_Longwave_radiation_emitted}{Longwave radiation emitted by
#'   the building.}
#'   \item{Greening_Temperature_Leafs}{Leaf temperature.}
#'   \item{Greening_Air_Temperature_Canopy}{Air temperature of the canopy.}
#'   \item{Greening_Air_Humidity_Canopy}{Air humidity of the canopy.}
#'   \item{Greening_Longwave_radiation_emitted}{Longwave radiation emitted by
#'   the greening.}
#'   \item{Greening_Wind_Speed_in_front_of_greening}{Wind speed in front of
#'   greening.}
#'   \item{Greening_Air_Temperature_in_front_of_greening}{Air temperature in
#'   front of greening.}
#'   \item{Greening_Shortwave_radiation_received_at_greening}{Shortwave
#'   radiation received by the greening.}
#'   \item{Greening_Incoming_longwave_radiation}{Incoming longwave radiation to
#'   the greening.}
#'   \item{Greening_Reflected_shortwave_radiation}{Reflected shortwave radiation
#'   by the greening.}
#'   \item{Greening_Transpiration_Flux}{Transpiration flux.}
#'   \item{Greening_Stomata_Resistance}{Stomata resistance.}
#'   \item{Greening_Water_access_factor}{Water access factor.}
#'   \item{Substrate_Temperature_Node_1_outside}{Temperature of the substrate at
#'   the interface between the first material and the atmosphere.}
#'   \item{Substrate_Temperature_Node_2}{Temperature of the substrate in the
#'   middle of the first material.}
#'   \item{Substrate_Temperature_Node_3}{Temperature of the substrate at the
#'   interface betweeen the first and the second material.}
#'   \item{Substrate_Temperature_Node_4}{Temperature of the substrate in the
#'   middle of the second material.}
#'   \item{Substrate_Temperature_Node_5}{Temperature of the substrate at the
#'   interface betweeen the second and the third material.}
#'   \item{Substrate_Temperature_Node_6}{Temperature of the substrate in the
#'   middle of the third material.}
#'   \item{Substrate_Temperature_Node_7_inside}{Temperature of the substrate at
#'   the interface between the third material and the wall.}
#'   \item{Substrate_Surface_humidity}{Surface humidity of the substrate.}
#'   \item{Substrate_Humidity_Flux_at_substrate}{Humidity flux at the substrate.}
#'   \item{Substrate_Longwave_radiation_emitted_by_substrate}{Longwave radiation
#'   emitted by the substrate.}
#'   \item{Substrate_Wind_Speed_in_front_of_substrate}{Wind speed in front of
#'   the substrate.}
#'   \item{Substrate_Air_Temperature_in_front_of_substrate}{Air temperature
#'   in front of the substrate.}
#'   \item{Substrate_Shortwave_radiation_received_at_substrate}{Shortwave
#'   radiation received by the substrate.}
#'   \item{Substrate_Absorbed_direct_shortwave_radiation}{Absorbed direction
#'   shortwave radiation by the substrate.}
#'   \item{Substrate_Incoming_longwave_radiation}{Incoming longwave radiation to
#'   the substrate.}
#'   \item{Substrate_Reflected_shortwave_radiation_substrate}{Reflected
#'   shortwave radiation by the substrate.}
#' }
#' @source \url{http://envi-met.info/doku.php?id=filereference:output:buildings}
#'
#' @aliases data_buildings buildings
#' @keywords data buildings
"data_buildings"
