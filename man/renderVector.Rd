% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/EDX.R
\name{renderVector}
\alias{renderVector}
\title{XML node data rendering as vector}
\usage{
renderVector(xmlFile, xmlNode, typeOfOutput = "character")
}
\arguments{
\item{xmlFile}{The XML file to read,}

\item{xmlNode}{the XML node where the data are extracted from}

\item{typeOfOutput}{The type of the returned vector.}
}
\value{
A vector which can contain strings or numbers.
}
\description{
Returns a vector containing data of a XML-like ENVI-met file (.EDX) node.
}
\details{
\code{renderVector} extracts the data from a node to return it as a vector.
The type of the returned vector depends on the one selected (numeric or
character).
}
\keyword{internal}
